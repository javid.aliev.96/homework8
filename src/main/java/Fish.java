
public class Fish extends Pet {
    public Fish(String nickname, int ageOfPet, int trickLevel, String[] habits) {
        super(nickname, ageOfPet, trickLevel, habits);
        super.setSpecies(Species.Fish);
    }


    @Override
    public void respond() {
        System.out.println("Hello owner. I am a Fish and my name is " + getNickname() + ". I miss you!");
    }
}
