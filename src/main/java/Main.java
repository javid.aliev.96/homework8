
public class Main {
    public static void main(String[] args) {
        Dog rex = new Dog("Rex", 5, 55, new String[]{"eat, sleep, run"});
        rex.respond();
        rex.foul();
        rex.eat();
        System.out.println(rex.toString());             // please note that in Pet class habits are stored in Set interface
        System.out.println();
        System.out.println(rex.getHabits().toString()); // verifying that Set performs correctly

        DomesticCat garfield = new DomesticCat("Garfield", 3, 99, new String[]{"trick Rex, take a nap, play with curtains"});
        System.out.println();

        Human test1 = new Human("Test", "Human", 1970, 99);
        test1.scheduleSetter(DayOfWeek.Monday.name(), "go to courses; watch a film");
        test1.scheduleSetter(DayOfWeek.Tuesday.name(), "do workout");
        test1.scheduleSetter(DayOfWeek.Wednesday.name(), "read e-mails");
        test1.scheduleSetter(DayOfWeek.Thursday.name(), "do shopping");
        test1.scheduleSetter(DayOfWeek.Friday.name(), "do household");
        test1.scheduleSetter(DayOfWeek.Saturday.name(), "visit grandparents");
        test1.scheduleSetter(DayOfWeek.Sunday.name(), "do homework");

        System.out.println(test1.toString());  // verifying that LinkedHashList of Map performs correctly

        Human test2 = new Human("Test", "Human", 1970, 99);

        System.out.println();
        Family testFamily = new Family(test1, test2);
        testFamily.setPet(rex);
        testFamily.setPet(garfield);
        testFamily.getPetList();                // verifying that Set list of Pets is working correctly

        System.out.println();
        Man childSon = new Man("Son", "Test1", 1996, rex);

        childSon.setFamily(testFamily);
        childSon.greetPet();
        childSon.fedUp();

        System.out.println();
        Woman childDaughter = new Woman("Daughter", "Test1", 1997, garfield);
        childDaughter.setFamily(testFamily);
        childDaughter.whine();
        childDaughter.greetPet();

        childDaughter.scheduleSetter(DayOfWeek.Monday.name(), "go to courses; watch a film");
        childDaughter.scheduleSetter(DayOfWeek.Tuesday.name(), "do workout");
        childDaughter.scheduleSetter(DayOfWeek.Wednesday.name(), "read e-mails");
        childDaughter.scheduleSetter(DayOfWeek.Thursday.name(), "do shopping");
        childDaughter.scheduleSetter(DayOfWeek.Friday.name(), "do household");
        childDaughter.scheduleSetter(DayOfWeek.Saturday.name(), "visit grandparents");
        childDaughter.scheduleSetter(DayOfWeek.Sunday.name(), "do homework");

        childDaughter.scheduleGetter();

        System.out.println();
        testFamily.addChild(childDaughter);
        testFamily.addChild(childSon);

        testFamily.countFamily();

        testFamily.deleteChild(childDaughter);
        testFamily.countFamily();
        testFamily.deleteChild(0);
        testFamily.countFamily();

    }
}




