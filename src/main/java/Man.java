
public final class Man extends Human {
    private Pet pet;

    public Man(String name, String surname, int year, Pet pet) {
        super(name, surname, year);
        this.pet = pet;
    }

    @Override
    public void scheduleSetter(String dayOfWeek, String tasks) {
        super.scheduleSetter(dayOfWeek, tasks);
    }

    @Override
    public void scheduleGetter() {
        super.scheduleGetter();
    }

    public void fedUp() {
        System.out.println("What is wrong again?!");
    }

    @Override
    public void greetPet() {
        System.out.println("Hey buddy, " + this.pet.getNickname() + ", come here.");
    }
}