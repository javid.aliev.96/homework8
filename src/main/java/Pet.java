import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int ageOfPet;
    private int trickLevel;
    private Set<String> habits;

    public Pet(String nickname, int ageOfPet, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.ageOfPet = ageOfPet;
        this.trickLevel = trickLevel;
        this.habits = new HashSet<>(Arrays.asList(habits));
    }

    public abstract void respond();

    public void eat() {

        System.out.println("I am eating!");
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {

        return nickname;
    }

    public void setNickname(String nickname) {

        this.nickname = nickname;
    }

    public int getAgeOfPet() {

        return ageOfPet;
    }

    public void setAgeOfPet(int ageOfPet) {

        this.ageOfPet = ageOfPet;
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {

        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {

        return habits;
    }

    public void setHabits(HashSet habits) {

        this.habits = habits;
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" +
                nickname +
                ", age=" +
                ageOfPet +
                ", trickLevel="
                + trickLevel +
                ", habits=" + habits.toString() +
                '}';
    }
}


