import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    public Family family;
    private Human father;
    private Human mother;
    private Human testch1;
    private Human testch2;
    private Human testch3;

    @BeforeEach
    void familyInfo() {
        mother = new Human("Anna", "Barzini", 1891);
        father = new Human("Anna", "Barzini", 1891);
        family = new Family(mother, father);
        DomesticCat emilioPet = new DomesticCat("Loona", 2, 30, new String[]{"run, take a nap, hide"});
        family.setPet(emilioPet);
        testch1 = new Man("TestChild1", "BLAH", 1996, emilioPet);
        testch2 = new Man("TestChild2", "BLAHBLAH", 1997, emilioPet);
        testch3 = new Woman("TestChild3", "BLAHBLAHBLAH", 1998, emilioPet);
        family.addChild(testch1);
        family.addChild(testch2);
    }

    @Test
    void addChild() {
        family.addChild(testch3);
        assertTrue(family.getChildrenList().contains(testch3));
    }

    @Test
    void deleteExistingObjectChild() {
        assertTrue(family.deleteChild(testch1));
        assertFalse(family.getChildrenList().contains(testch1));
    }

    @Test
    void deleteNonExistingObjectChild() {
        assertFalse(family.deleteChild(testch3));
    }

    @Test
    void deleteExistingIndexChild() {
        assertTrue(family.deleteChild(0));
        assertFalse(family.getChildrenList().contains(testch1));
    }

    @Test
    void deleteNonExistingIndexChild() {
        assertFalse(family.deleteChild(10));
    }

    @Test
    void countFamily() {
        int result = family.countFamily();
        assertEquals(4, result);
    }

    @Test
    void testToString() {
        String expectedString = "Family{mother=Human{name='Anna', surname='Barzini', year=1891, iq=0, schedule={}}, " +
                "father=Human{name='Anna', surname='Barzini', year=1891, iq=0, schedule={}}, " +
                "pet=DomesticCat{nickname='Loona, age=2, trickLevel=30, habits=[run, take a nap, hide]}, " +
                "childrenList=[Human{name='TestChild1', surname='BLAH', year=1996, iq=0, schedule={}}, " +
                "Human{name='TestChild2', surname='BLAHBLAH', year=1997, iq=0, schedule={}}]}";

        assertEquals(expectedString, family.toString());
    }
}